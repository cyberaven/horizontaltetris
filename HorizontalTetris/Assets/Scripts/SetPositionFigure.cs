﻿using UnityEngine;
using System.Collections;

public class SetPositionFigure : MonoBehaviour, ISetPosition
{
    public void SetPosition(float x, float z)
    {
        transform.position = new Vector3((int)x, 1, (int)z);
    }
}
