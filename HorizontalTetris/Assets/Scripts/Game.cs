﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    static public Game Instance;
    
    [SerializeField] FigureHandler figureHandler;

    [SerializeField] Field field;
    [SerializeField] int fieldSizeX = 5;
    [SerializeField] int fieldSizeZ = 5;

    private void Awake()
    {
        Instance = this;                
    }
    void Start()
    {
        Init();        
    }    

    void Init()
    {        
        figureHandler = Instantiate(figureHandler, transform);

        CreateField(fieldSizeX, fieldSizeZ);
    }

    void CreateField(int x, int z)
    {
        field = Instantiate(field,transform);
        field.CreateField(x, z);
    }


    /*Создание фигур
     * --Кликаем по ячейке карты
     * --Создаем фигуру
     * --Создаем карту фигуры
     * --проверяем можно ли положить сюда фигуру +
     * --кладем в ячейку фигуру++
     * --
     * --
     * 
     * 
     * Движение фигур
     * --поле получает ячейку в которую движится фигура
     * --проверяет новое место на занятость с помощью карты фигуры +
     * --разрешает/запрещает движение++
     * 
     * 
     * 
     * 
     * 
     * говорим полю карту фигуры и активную ячейку
     * поле заполняет занятые ячейки "занято"
     * кладем фигуру на поле
     * при движении фигуры
     * г
     * 
     * 
     * */
}
