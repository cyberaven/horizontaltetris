﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureHandler : MonoBehaviour
{
    static public FigureHandler Instance;

    [SerializeField] List<Figure> figurePrefabs;    
    [SerializeField] List<Figure> figureOnField;    

    private void Awake()
    {
        Instance = this;
    }   
    public List<Figure> GetAllFigurePrefabs()
    {
        return figurePrefabs;
    }

    public void CreateFigure(int x, int z)
    {
        Figure figure = NewRandFigure();
        figure.transform.SetParent(Field.Instance.transform);
        figure.SetPosition(x, z);        
        figureOnField.Add(figure);
    }

    void CreateFigureMap(Figure figure)
    {
        Transform[] figureChilds = figure.GetComponentsInChildren<Transform>();
        foreach(Transform figureChild in figureChilds)
        {        
            figure.SetMap(figureChild.localPosition);
        }        
    }
    
    public Figure NewRandFigure()
    {
        Figure figure = Instantiate(figurePrefabs[Random.Range(0, figurePrefabs.Count)]);
        CreateFigureMap(figure);
        return figure;
    }
}
