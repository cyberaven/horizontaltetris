﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    static public Field Instance;

    [SerializeField] FieldCell fieldCell;
    [SerializeField] List<FieldCell> fieldCells;

    [SerializeField] int sizeX = 0;
    [SerializeField] int sizeZ = 0;
    private void Awake()
    {
        Instance = this;
    }
    public void CreateField(int x, int z)
    {
        SetSize(x, z);
        FillField();               
    }
    void SetSize(int x, int z)
    {
        sizeX = x;
        sizeZ = z;
    }
    void FillField()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                fieldCell = Instantiate(fieldCell, transform);
                fieldCells.Add(fieldCell);
                fieldCell.name = "FC:" + x + ":" + z;
                FieldCellSetPosition(fieldCell, x, z);
            }
        }
    }    
    void FieldCellSetPosition(FieldCell fieldCell, int x, int z)
    {
        fieldCell.SetPosition(x,z);
    }

    public void FieldCellWantNewFigure(FieldCell fieldCell)
    {
        Debug.Log("FieldCellWantNewFigure:"+fieldCell);
        Figure figure = FigureHandler.Instance.NewRandFigure();
        Debug.Log("NewFigure" + figure);

        if (TryPutFigureInFieldCell(fieldCell, figure))
        {
            fieldCell.PutFigure(figure);
            FillFieldCell(fieldCell, figure, true);
            figure.SetPosition((int)fieldCell.transform.position.x, (int)fieldCell.transform.position.z);
        }
        else
        {
            Destroy(figure.gameObject);
            FieldCellWantNewFigure(fieldCell);
        }
    }

    public bool TryPutFigureInFieldCell(FieldCell fieldCell, Figure figure)
    {
        if (fieldCell.empty == true)
        {
            Vector3 fcPos = fieldCell.transform.localPosition;
            List<Vector3> fMap = figure.childsPos;

            for (int i = 1; i < fMap.Count; i++)
            {
                Vector3 fieldCellPos = fcPos + fMap[i];
                FieldCell fc = GetFieldCell(fieldCellPos);

                if (fc == null || fc.empty == false)
                {
                    Debug.Log(false);
                    return false;
                }
            }
            Debug.Log(true);
            return true;
        }
        else
        {
            Debug.Log(false);
            return false;
        }
    }
    public FieldCell GetFieldCell(Vector3 fieldCellPos)
    {
        FieldCell needFieldCell = null;

        foreach(FieldCell fieldCell in fieldCells)
        {
            Debug.Log("needFieldCell11 tp: " + fieldCell.transform.localPosition);
            Debug.Log("needFieldCell11 fcpos: " + fieldCellPos);

            if (fieldCell.transform.localPosition == fieldCellPos)
            {
                needFieldCell = fieldCell;
                Debug.Log("needFieldCell "+ needFieldCell);
                return needFieldCell;
            }            
        }
        Debug.Log("needFieldCell " + needFieldCell);
        return needFieldCell;
    }

    public void FillFieldCell(FieldCell fieldCell, Figure figure, bool empty)
    {
        Vector3 fcPos = fieldCell.transform.localPosition;
        List<Vector3> fMap = figure.childsPos;

        for (int i = 1; i < fMap.Count; i++)
        {
            Vector3 fieldCellPos = fcPos + fMap[i];
            FieldCell fc = GetFieldCell(fieldCellPos);
            fc.empty = empty;
        }

    }
}
