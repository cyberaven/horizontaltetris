﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    ISetPosition setPosition;

    public float distance = 8; 
    Rigidbody rb;

    [SerializeField] Vector3 oldPos;
    [SerializeField] Vector3 newPos;   

    [SerializeField] public List<Vector3> childsPos;

    void Awake()
    {
        setPosition = gameObject.AddComponent<SetPositionFigure>();
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void SetPosition(float x, float z)
    {
        setPosition.SetPosition(x, z);
    }
   
    public void SetMap(Vector3 childPos)
    {
        childsPos.Add(childPos);
    }


    Vector3 SavePos()
    {
        return oldPos = transform.position;        
    }
    Vector3 NewPos()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);            
        return objPosition;                
    }
    private bool CheckPos(Vector3 newPos)
    {
        Debug.Log("----------newPos:" + newPos);

        newPos = new Vector3((int)newPos.x, 0, (int)newPos.z);

        Debug.Log("----------newPosInt:" + newPos);

        FieldCell fieldCell = Field.Instance.GetFieldCell(newPos);

        if(Field.Instance.TryPutFigureInFieldCell(fieldCell, this))
        {
            return true;
        }
        else
        {
            return false;
        }        
    }
    void FillFieldCellEmpty()
    {

    }
    private void OnMouseDrag()
    {
        oldPos = SavePos();
        newPos = NewPos();        
        if (CheckPos(newPos))
        {

            FieldCell fieldCell = Field.Instance.GetFieldCell(oldPos);
            Field.Instance.FillFieldCell(fieldCell, this, false);

            SetPosition(newPos.x, newPos.z);
        }
        else
        {
            

            SetPosition(oldPos.x, oldPos.z);
        }
    }    
}
