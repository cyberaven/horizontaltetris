﻿using UnityEngine;
using System.Collections;

public class SetPositionFieldCell : MonoBehaviour, ISetPosition
{
    public void SetPosition(float x, float z)
    {
        transform.localPosition = new Vector3((int)x, 0, (int)z);
    }
}
