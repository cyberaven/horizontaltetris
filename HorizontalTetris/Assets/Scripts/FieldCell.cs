﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldCell : MonoBehaviour
{
    public bool empty = true;
    [SerializeField] Figure figure;

    ISetPosition setPosition;

    void Awake()
    {
        setPosition = gameObject.AddComponent<SetPositionFieldCell>();
    }

    public void SetPosition(int x, int z)
    {
        setPosition.SetPosition(x,z);        
    }
    public void PutFigure(Figure setFigure)
    {
        figure = setFigure;
    }

    private void OnMouseDown()
    {
        //FigureHandler.Instance.CreateFigure((int)transform.localPosition.x, (int)transform.localPosition.z);
        Field.Instance.FieldCellWantNewFigure(this);
    }
}
